# Demo-Form

For demonstration/test purpose only.

When you install this service, a demonstration form will display many parameters to demonstrate the supported input
types and their options.

The launched application is Echo Server, which is useful to view the variables received from the install form.

More documentation here: https://github.com/Ealenn/Echo-Server
